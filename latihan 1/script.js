var nama = ["Ali Imron", "Puji Ayu Lestari", "Fikri Al Hakim"]
var umur = ["23", "26", "24"]

var gabungan = nama.concat(umur);
gabungan.splice(3,0, "1997", "1994", "1996");

function cetak(){ //fungsi cetak array
    for (var i = 0; i < gabungan.length; i++) { //Looping teks sebanyak jumlah elemen pada array
        console.log(gabungan[i]);
    }
}

var cetak_anggota = cetak();
console.log(cetak_anggota);
